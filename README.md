# backoffice-app

Backoffice app dedicated to an online shop’s employees, where they can create, display and delete products from the office or on the go during their work travels.

Per testare l'applicazione:
* installre i moduli con il comando `yarn`
* aggiungere alla directory il file `env.local` allegato alla e-mail
* eseguire il comando `yarn start`