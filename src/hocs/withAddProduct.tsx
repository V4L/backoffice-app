import AddProductForm from '../forms/AddProductForm';
import {
  Backdrop,
  Container,
  makeStyles,
  Modal
} from '@material-ui/core';
import { FC, useState } from 'react';

const useStyle = makeStyles({
  formContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 4,
    marginBottom: 10,
    marginTop: 10,
    maxWidth: 425,
    overflow: 'auto',
    padding: 20,
    width: '90vw',
    ['@media only screen and (max-height: 480px)']: {
      maxHeight: 460
    }
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

interface WrappedComponetProps {
  addProduct: () => void;
}

export default function withAddProduct(WrappedComponent: FC<WrappedComponetProps>) {
  function WithAddProduct() {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const classes = useStyle();

    const openModal = () => setIsModalOpen(true);
    const closeModal = () => setIsModalOpen(false);

    return (
      <>
        <WrappedComponent addProduct={openModal} />
        <Modal
          className={classes.modal}
          open={isModalOpen}
          onClose={closeModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{ timeout: 500 }}
        >
          <Container className={classes.formContainer}>
            <AddProductForm onAddProduct={closeModal} />
          </Container>
        </Modal>
      </>
    )
  }

  return WithAddProduct
}
