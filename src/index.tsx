import App from './App';
import { PersistGate } from 'redux-persist/integration/react';
import ReactDOM from 'react-dom';
import { store, persistor } from './redux/store';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';

import './index.scss';

ReactDOM.render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
