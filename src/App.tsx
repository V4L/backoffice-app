import Header from './components/Header';
import MainContainer from './containers/MainContainer';
import NavBar from './components/NavBar';
import Navigation from './components/Navigation';
import ScreenLoader from './components/ScreenLoader';
import { BrowserRouter as Router } from 'react-router-dom';
import { Suspense } from 'react';

export default function App() {
  return (
    <Suspense fallback={<ScreenLoader modifier="full-page" />}>
      <Router>
        <Header />
        <MainContainer>
          <NavBar />
          <Navigation />
        </MainContainer>
      </Router>
    </Suspense>
  );
}
