import { Product } from "../redux/types"

export const getFavoritesFromStorage = () => {
  const favorites = localStorage.getItem('favorites');
  return favorites ? JSON.parse(favorites) : null;
}

export const addFavoritesToStorage = (product: Product) => {
  const favorites = getFavoritesFromStorage();
  if (!favorites) {
    localStorage.setItem('favorites', JSON.stringify([product]));
  } else {
    localStorage.setItem('favorites', JSON.stringify([...favorites, product]));
  }
}

export const removeFavoritesFromStorage = (id: string) => {
  const favorites = getFavoritesFromStorage();
  const newFavorites = favorites.filter((f: Product) => f.id !== id);
  localStorage.setItem('favorites', JSON.stringify(newFavorites));
}