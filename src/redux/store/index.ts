import * as localforage from 'localforage';
import logger from 'redux-logger';
import { persistReducer, persistStore } from 'redux-persist';
import rootReducer from '../reducers';
import thunk from 'redux-thunk';
import { applyMiddleware, createStore, compose, Action } from 'redux';

const persistConfig = {
  key: 'root',
  storage: localforage
}

export type RootState = ReturnType<typeof rootReducer>;

const persistedReducer = persistReducer<RootState, any>(persistConfig, rootReducer);

export const store = createStore(
  persistedReducer,
  {},
  compose(applyMiddleware(thunk, logger))
);


export const persistor = persistStore(store);
