import { SET_ERROR, SetErrorAction } from '../types';

export const setError = (message: string): SetErrorAction => ({
  type: SET_ERROR,
  payload: {
    message
  }
});
