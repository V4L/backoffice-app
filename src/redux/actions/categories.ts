import { Categories, CategoriesActionType, SET_CATEGORIES } from '../types'

export const setCategories = (categories: Categories): CategoriesActionType => ({
  type: SET_CATEGORIES,
  payload: {
    categories
  }
});
