import axios from 'axios';
import { addProduct, deleteProduct, setProducts } from './products';
import { AppThunk, FetchedProduct, FetchedProducts, Product } from '../types';
import { fetchingProducts } from './fetchingProducts';
import { setCategories } from './categories';
import { setError } from './errors';
import { setShopInfo } from './shopInfo';

const getRandomRatings = () => Math.floor(Math.random() * 5) + 1;

const formatProductsList = (data: FetchedProducts) => data.reduce(
  (newData: Product[], currentValue: FetchedProduct) => [
    ...newData,
    { ...currentValue.data, id: currentValue.id, rating: getRandomRatings() }
  ], []
)

const baseErrorMsg = 'An error occurred while';
const baseURL = process.env.REACT_APP_BASE_URL;

export const fetchProducts = (): AppThunk => async (dispatch) => {
  try {
    dispatch(fetchingProducts(true));
    const response = await axios.get(`${baseURL}/products`);
    const list = formatProductsList(response.data);
    const length = response.data.length;
    dispatch(setProducts({ list, length }));
    dispatch(fetchingProducts(false));
  } catch (error) {
    dispatch(fetchingProducts(false));
    dispatch(setError(`${baseErrorMsg} fetching products.`));
    console.error(error);
  }
}

export const createProduct = (data: Product): AppThunk => async (dispatch) => {
  try {
    const response = await axios.post(`${baseURL}/products`, data);
    const product = { ...data, id: response.data, rating: getRandomRatings() };
    dispatch(addProduct(product));
  } catch (error) {
    dispatch(setError(`${baseErrorMsg} creating ${data.title} product.`));
    console.error(error);
  }
}

export const removeProduct = (id: string): AppThunk => async (dispatch) => {
  try {
    const response = await axios.delete(`${baseURL}/products/${id}`);
    dispatch(deleteProduct(id));
    return response;
  } catch (error) {
    dispatch(setError(`${baseErrorMsg} deleting the product.`));
    console.error(error);
  }
}

export const fetchCategories = (): AppThunk => async (dispatch) => {
  try {
    const response = await axios.get(`${baseURL}/stats/categories`);
    const categories = response.data;
    dispatch(setCategories(categories));
  } catch (error) {
    dispatch(setError(`${baseErrorMsg} fetching categories.`));
    console.error(error);
  }
}

export const fetchShopInfo = (): AppThunk => async (dispatch) => {
  try {
    const response = await axios.get(`${baseURL}`);
    const shopInfo = response.data;
    dispatch(setShopInfo(shopInfo));
  } catch (error) {
    dispatch(setError(`${baseErrorMsg} fetching shop information.`))
    console.error(error);
  }
}
