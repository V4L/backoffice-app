import { CHANGE_PRODUCTS_VIEW, ChangeProductsViewAction, ProductsView } from '../types';

export const changeProductsView = (view: ProductsView): ChangeProductsViewAction => ({
  type: CHANGE_PRODUCTS_VIEW,
  payload: {
    view
  }
});
