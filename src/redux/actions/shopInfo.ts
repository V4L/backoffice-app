import { SET_SHOP_INFO, ShopInfo, ShopInfoActionType } from '../types';

export const setShopInfo = (info: ShopInfo): ShopInfoActionType => ({
  type: SET_SHOP_INFO,
  payload: {
    info
  }
});
