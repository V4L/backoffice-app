import { SET_FILTER_QUERY, SetFilterQueryAction } from '../types';

export const setFilterQuery = (payload: string): SetFilterQueryAction => ({
  type: SET_FILTER_QUERY,
  payload
});