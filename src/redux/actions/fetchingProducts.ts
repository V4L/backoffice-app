import { FETCHING_PRODUCTS, FetchingProducts, FetchingProductsAction } from '../types';

export const fetchingProducts = (fetching: FetchingProducts): FetchingProductsAction => ({
  type: FETCHING_PRODUCTS,
  payload: {
    fetching
  }
});
