import {
  ADD_PRODUCT,
  DELETE_PRODUCT,
  Product,
  Products,
  ProductsActionTypes,
  SET_PRODUCTS
} from '../types'

export const setProducts = (products: Products): ProductsActionTypes => ({
  type: SET_PRODUCTS,
  payload: {
    products
  }
});

export const addProduct = (product: Product) => ({
  type: ADD_PRODUCT,
  payload: {
    product
  }
});

export const deleteProduct = (id: string) => ({
  type: DELETE_PRODUCT,
  payload: {
    id
  }
});
