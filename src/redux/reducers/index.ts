import { combineReducers } from 'redux';

import categories from './categories';
import errors from './errors';
import fetchingProducts from './fetchingProducts';
import products from './products';
import productsView from './productsView';
import shopInfo from './shopInfo';
import filterQuery from './filterQuery'

const rootReducer = combineReducers({
  categories,
  errors,
  fetchingProducts,
  products,
  productsView,
  shopInfo,
  filterQuery
});

export default rootReducer;
