import { FETCHING_PRODUCTS, FetchingProducts, FetchingProductsAction } from '../types';

const initialState: FetchingProducts = false;

export default (state = initialState, action: FetchingProductsAction) => {
  switch (action.type) {
    case FETCHING_PRODUCTS:
      return action.payload.fetching
    default:
      return state;
  }
}