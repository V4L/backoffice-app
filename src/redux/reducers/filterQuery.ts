import { SET_FILTER_QUERY, SetFilterQueryAction } from '../types';

const initialState = '';


export default (state = initialState, action: SetFilterQueryAction) => {
  switch (action.type) {
    case SET_FILTER_QUERY:
      return action.payload;
    default:
      return state;
  }
}
