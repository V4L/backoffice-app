import uniqBy from 'lodash/uniqBy';
import {
  ADD_PRODUCT,
  DELETE_PRODUCT,
  SET_PRODUCTS,
  Products,
  ProductsActionTypes
} from '../types';

const initialState: Products = {
  list: [],
  length: 0
};

export default (state = initialState, action: ProductsActionTypes) => {
  switch (action.type) {
    case SET_PRODUCTS:
      return {
        ...state,
        list: uniqBy([
          ...state.list,
          ...action.payload.products.list
        ], 'id'),
        length: action.payload.products.length
      }
    case ADD_PRODUCT:
      return {
        ...state,
        list: [
          action.payload.product,
          ...state.list
        ],
        length: state.length + 1
      }
    case DELETE_PRODUCT:
      const index = state.list.findIndex((product) => product.id === action.payload.id);
      return {
        ...state,
        list: [
          ...state.list.slice(0, index),
          ...state.list.slice(index + 1)
        ],
        length: state.length - 1
      }
    default:
      return state;
  }
}
