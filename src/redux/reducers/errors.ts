import { SET_ERROR, SetErrorAction } from '../types';

const initialState = '';


export default (state = initialState, action: SetErrorAction) => {
  switch (action.type) {
    case SET_ERROR:
      return action.payload.message;
    default:
      return state;
  }
}
