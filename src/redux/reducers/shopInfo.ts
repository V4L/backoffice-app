import {
  SET_SHOP_INFO,
  ShopInfo,
  ShopInfoActionType
} from '../types';

const initialState: ShopInfo = {
  category: '',
  employees: [],
  name: ''
};

export default (state = initialState, action: ShopInfoActionType) => {
  switch (action.type) {
    case SET_SHOP_INFO:
      return action.payload.info;
    default:
      return state;
  }
}
