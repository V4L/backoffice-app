import {
  Categories,
  CategoriesActionType,
  SET_CATEGORIES
} from '../types';

const initialState: Categories = []

export default (state = initialState, action: CategoriesActionType) => {
  switch (action.type) {
    case SET_CATEGORIES:
      return action.payload.categories;
    default:
      return state;
  }
}
