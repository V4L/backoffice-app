import { CHANGE_PRODUCTS_VIEW, ChangeProductsViewAction, ProductsView } from '../types';

const initialState: ProductsView = 'grid';


export default (state = initialState, action: ChangeProductsViewAction) => {
  switch (action.type) {
    case CHANGE_PRODUCTS_VIEW:
      return action.payload.view;
    default:
      return state;
  }
}
