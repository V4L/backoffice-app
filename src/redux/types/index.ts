import { Action } from 'redux';
import { RootState } from '../store';
import { ThunkAction } from 'redux-thunk';

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>

/* SHOP INFO */

export interface ShopInfo {
  category: string;
  employees: string[];
  name: string;
}

export const SET_SHOP_INFO = 'SET_SHOP_INFO';

interface SetShopInfoAction {
  type: typeof SET_SHOP_INFO;
  payload: {
    info: ShopInfo;
  }
}

export type ShopInfoActionType = SetShopInfoAction;

/* CATEGORIES */

export interface Category {
  category: string;
  numberOfProducts: number;
}

export type Categories = Category[];

export const SET_CATEGORIES = 'SET_CATEGORIES';

interface SetCategoriesAction {
  type: typeof SET_CATEGORIES;
  payload: {
    categories: Categories;
  }
}

export type CategoriesActionType = SetCategoriesAction;

/* PRODUCTS */

export interface Product {
  id: string;
  title: string;
  description: string;
  employee: string;
  price: string;
  category: string;
  rating: number;
}

export interface Products {
  list: Product[];
  length: number;
}

export interface FetchedProduct {
  id: string;
  data: Product;
}

export type FetchedProducts = FetchedProduct[];

export const ADD_PRODUCT = 'ADD_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const SET_PRODUCTS = 'SET_PRODUCTS';

interface SetProductsAction {
  type: typeof SET_PRODUCTS;
  payload: {
    products: Products;
  }
}

interface AddProductAction {
  type: typeof ADD_PRODUCT;
  payload: {
    product: Product;
  }
}

interface DeleteProductAction {
  type: typeof DELETE_PRODUCT;
  payload: {
    id: string;
  }
}

export type ProductsActionTypes = SetProductsAction | AddProductAction | DeleteProductAction;

/* FETCHING PRODUCTS */

export type FetchingProducts = boolean;

export const FETCHING_PRODUCTS = 'FETCHING_PRODUCTS';

export interface FetchingProductsAction {
  type: typeof FETCHING_PRODUCTS;
  payload: {
    fetching: FetchingProducts;
  }
}

/* PRODUCTS VIEW */

export type ProductsView = 'panel' | 'grid';

export const CHANGE_PRODUCTS_VIEW = 'CHANGE_PRODUCTS_VIEW';

export interface ChangeProductsViewAction {
  type: typeof CHANGE_PRODUCTS_VIEW;
  payload: {
    view: ProductsView;
  }
}

/* FILTER QUERY */

export const SET_FILTER_QUERY = 'SET_FILTER_QUERY'

export interface SetFilterQueryAction {
  type: typeof SET_FILTER_QUERY;
  payload: string
}

/* ERRORS */

export const SET_ERROR = 'FETCH_PRODUCTS_ERROR';

export interface SetErrorAction {
  type: typeof SET_ERROR;
  payload: {
    message: string;
  }
}
