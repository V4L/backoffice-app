import { Button, FormGroup, makeStyles, TextField } from '@material-ui/core';
import { createProduct } from '../redux/actions/thunks';
import { Product } from '../redux/types';
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';

const useStyle = makeStyles({
  borders: {
    borderBottom: `2px solid #efa345`,
    '&$error': {
      borderBottomColor: '#f00000'
    },
    '&:after': {
      borderBottom: `2px solid #efa345`
    },
  },
  label: {
    '&$focusedLabel': {
      color: '#efa345'
    },
    '&$erroredLabel': {
      color: '#f00000'
    }
  },
  error: {},
  focused: {},
  focusedLabel: {},
  erroredLabel: {},
  fields: {
    fontFamily: 'inherit',
    fontSize: 16,
    height: 80,
    paddingBottom: 20,
    width: '80%',
    ['@media only screen and (min-width: 500px)']: {
      width: '60%'
    }
  },
  formGroup: {
    alignItems: 'center',
    paddingTop: 10,
    overflow: 'auto'
  },
  submit: {
    backgroundColor: '#efa345',
    '&:hover': {
      backgroundColor: '#efa345'
    },
    color: '#ffffff',
    fontWeight: 'bold',
    margin: 20
  },
  textarea: {
    height: 'auto'
  }
});

type Inputs = Product;
interface FormProps {
  onAddProduct: () => void;
}

export default function AddProductForm({ onAddProduct }: FormProps) {
  const { register, handleSubmit, reset, errors } = useForm<Inputs>();
  const dispatch = useDispatch();
  const classes = useStyle();

  const InputProps = {
    classes: {
      root: classes.borders,
      error: classes.error,
      focused: classes.focused
    },
    disableUnderline: true
  }

  const InputLabelProps = {
    classes: {
      root: classes.label,
      focused: classes.focusedLabel,
      error: classes.erroredLabel
    }
  }

  const onSubmit = (data: Inputs) => {
    dispatch(createProduct(data));
    reset();
    onAddProduct();
  }

  return (
    <form className="form" onSubmit={handleSubmit(onSubmit)}>
      <FormGroup className={classes.formGroup}>
        <TextField
          className={classes.fields}
          autoComplete="off"
          label="Title"
          name="title"
          error={!!errors.title}
          helperText={errors.title && `* ${errors.title?.type}`}
          FormHelperTextProps={{ margin: 'dense' }}
          inputRef={register({ required: true })}
          inputProps={{ ['data-lpignore']: true }}
          InputProps={InputProps}
          InputLabelProps={InputLabelProps}
        />
        <TextField
          className={`${classes.fields} ${classes.textarea}`}
          multiline
          rows={4}
          autoComplete="off"
          label="Description"
          name="description"
          error={!!errors.description}
          helperText={errors.description && `* ${errors.description?.type}`}
          inputRef={register({ required: true })}
          InputProps={InputProps}
          InputLabelProps={InputLabelProps}
        />
        <TextField
          className={classes.fields}
          autoComplete="off"
          label="Price (€/kg)"
          name="price"
          type="number"
          error={!!errors.price}
          helperText={errors.price && `* ${errors.price?.type}`}
          inputRef={register({ required: true })}
          inputProps={{ ['data-lpignore']: true }}
          InputProps={InputProps}
          InputLabelProps={InputLabelProps}
        />
        <TextField
          className={classes.fields}
          autoComplete="off"
          label="Employee"
          name="employee"
          error={!!errors.employee}
          helperText={errors.employee && `* ${errors.employee?.type}`}
          inputRef={register({ required: true })}
          inputProps={{ ['data-lpignore']: true }}
          InputProps={InputProps}
          InputLabelProps={InputLabelProps}
        />
        <TextField
          className={classes.fields}
          autoComplete="off"
          label="Category"
          name="category"
          error={!!errors.category}
          helperText={errors.category && `* ${errors.category?.type}`}
          inputRef={register({ required: true })}
          inputProps={{ ['data-lpignore']: true }}
          InputProps={InputProps}
          InputLabelProps={InputLabelProps}
        />
        <Button
          className={classes.submit}
          type="submit"
          variant="contained"
        >
          submit
        </Button>
      </FormGroup>
    </form>
  );
}
