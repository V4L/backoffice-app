import ReviewStars from '../components/Stars';
import {
  Avatar,
  Backdrop,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Chip,
  makeStyles,
  Modal,
  Paper,
  Typography
} from '@material-ui/core';
import { removeProduct } from '../redux/actions/thunks';
import { grey } from '@material-ui/core/colors';
import { Product } from '../redux/types';
import { RootState } from '../redux/store';
import { RouteComponentProps } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

import cakeImage from '../assets/images/cake.jpg';

const useStyle = makeStyles({
  avatar: {
    height: 50,
    width: 50
  },
  card: {
    alignItems: 'center',
    borderColor: grey[300],
    borderStyle: 'solid',
    borderWidth: 1,
    minHeight: '70vh',
    marginBottom: 10
  },
  cardContent: {
    textAlign: 'center'
  },
  cardMedia: {
    height: 200
  },
  category: {
    backgroundColor: '#efa345',
    color: '#ffffff',
    marginBottom: 20,
    marginTop: 20,
    minWidth: 100
  },
  deleteButton: {
    backgroundColor: '#F00000',
    '&:hover': {
      backgroundColor: '#f00000'
    },
    right: 5,
    top: 5
  },
  deleteModal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  deleteModalContent: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    height: 150,
    justifyContent: 'center',
    padding: 20,
    width: 250
  },
  deleteModalMessage: {
    fontSize: 18,
    marginBottom: 16
  },
  deleteModalOk: {
    backgroundColor: '#efa345',
    '&:hover': {
      backgroundColor: '#efa345'
    },
    color: '#ffffff',
    marginLeft: 6
  },
  deleteTitle: {
    fontStyle: 'italic',
    fontWeight: 'bold'
  },
  headerTitle: {
    fontSize: 13,
    fontStyle: 'italic',
    opacity: .5
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold'
  },
  price: {
    fontSize: 28,
    fontWeight: 'bold',
    marginTop: 20
  },
  subHeader: {
    color: '#e46d07',
    maxWidth: '90%'
  }
});

interface ProductPageProps {
  id: string;
}

const selectProductsList = (state: RootState) => state.products.list;

export default function ProductPage({ history, match }: RouteComponentProps<ProductPageProps>) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const classes = useStyle();
  const dispatch = useDispatch();
  const productsList = useSelector(selectProductsList);  
  const product = productsList.find((p: Product) => p.id === match.params.id);

  const {
    employee,
    title,
    description,
    price,
    category,
    rating
  } = product || {};

  useEffect(() => {
    if (productsList?.length > 0 && !product) {
      history.push('/404');
    }
  }, [product, productsList])

  const openModal = () => setIsModalOpen(true);
  const closeModal = () => setIsModalOpen(false);
  const handleDeleteProduct = () => {
    dispatch(removeProduct(match.params.id))
    history.push('/products');
  };

  return (
    <>
      <Card className={classes.card}>
        <CardHeader
          avatar={<Avatar className={classes.avatar}>{employee?.[0].toLowerCase()}</Avatar>}
          title={(
            <Typography component="p" className={classes.headerTitle}>
              created by
            </Typography>
          )}
          subheader={(
            <Typography
              component="p"
              className={classes.subHeader}
              noWrap
            >
              {employee}
            </Typography>
          )}
          action={(
            <Button
              className={classes.deleteButton}
              color="secondary"
              onClick={openModal}
              variant="contained"
            >
              delete
            </Button>
          )}
        />
        <CardMedia
          className={classes.cardMedia}
          component="img"
          src={cakeImage}
          title="Cake"
        />
        <CardContent className={classes.cardContent}>
          <Typography className={classes.title} component="p">
            {title?.toUpperCase()}
          </Typography>
          <Typography component="p">
            {description}
          </Typography>
          <Typography className={classes.price} component="p">
            {`€/kg ${price}`}
          </Typography>
          <Chip label={category?.toUpperCase()} className={classes.category} />
          {rating && <ReviewStars count={rating} />}
        </CardContent>
      </Card>
      <Modal
        className={classes.deleteModal}
        open={isModalOpen}
        onClose={closeModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{ timeout: 500 }}
      >
        <Paper className={classes.deleteModalContent}>
          <Typography className={classes.deleteModalMessage} component="p">
            Are you sure you want to delete
            {' '}
            <Typography className={classes.deleteTitle} component="span">{title}</Typography>
            ?
          </Typography>
          <Box>
            <Button
              onClick={closeModal}
              variant="outlined"
            >
              cancel
            </Button>
            <Button
              onClick={handleDeleteProduct}
              className={classes.deleteModalOk}
              variant="contained"
            >
              yes
            </Button>
          </Box>
        </Paper>
      </Modal>
    </>
  );
}
