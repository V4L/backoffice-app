export default function NoPageFound() {
  return (
    <p className="not-found">404 - Page Not Found</p>
  );
}
