import range from 'lodash/range';
import { fetchCategories } from '../redux/actions/thunks';
import { Polar } from 'react-chartjs-2';
import { RootState } from '../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';

const selectCategories = (state: RootState) => state.categories;

const getColor = () => {
  const randomColor = Math.floor(Math.random() * 16777215).toString(16);
  return `#${randomColor}`
};

export default function CategoriesPage() {
  const dispatch = useDispatch();
  const categories = useSelector(selectCategories);

  useEffect(() => {
    dispatch(fetchCategories());
  }, [])

  const backgroundColor = range(0, categories?.length).map(() => getColor());
  const borderColor = range(0, categories?.length).map(() => '#ffffff' );

  const data = {
    labels: categories?.map((c) => c.category),
    datasets: [
      {
        data: categories?.map((c) => c.numberOfProducts),
        backgroundColor, 
        borderColor
      }
    ]
  }

  return (
    <div>
      <Polar
        data={data}
        height={500}
        width={500}
        options={{
          legend: {
            align: 'start',
            labels: {
              boxWidth: 50,
              fontColor: '#e46d07',
              fontSize: 14
            }
          },
          maintainAspectRatio: false,
          title: {
            display: true,
            text: 'Categories Stats',
            fontColor: '#e46d07',
            fontSize: 20,
            lineHeight: 1
          }
        }}
      />
    </div>
  );
}
