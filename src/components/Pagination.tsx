import { Box } from '@material-ui/core';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';

type Props = {
  currentPage: number;
  totalPages: number;
  setPage: (p: number) => void;
  itemsPerPage: number;
}

export default function Pagination({ currentPage, setPage, totalPages, itemsPerPage }: Props) {
  return (
    <Box className='pagination-container'>
      <Box
        className='pagination-container__left-arrow'
        onClick={() => currentPage > 1 && setPage(currentPage - 1)}
      >
        <ChevronLeft />
      </Box>
      {Array.from({ length: totalPages }, (_, i) => {
        const number = i + 1
        return (
          <Box
            key={i}
            onClick={() => setPage(number)}
            className={`pagination-container__page pagination-container__page--${number === currentPage ? 'selected' : ''}`}
          >
            {number}
          </Box>
        )
      })}
      <Box
        className='pagination-container__right-arrow'
        onClick={() => currentPage < totalPages && setPage(currentPage + 1)}
      >
        <ChevronRight />
      </Box>
    </Box>
  )
}