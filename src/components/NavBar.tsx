import { AppBar, Box, Button, makeStyles, TextField, Toolbar } from '@material-ui/core';
import Clear from '@material-ui/icons/Clear';
import ForwardIcon from '@material-ui/icons/Forward';
import Search from '@material-ui/icons/Search';
import { Alert } from '@material-ui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, useLocation } from 'react-router-dom';
import { setError } from '../redux/actions/errors';
import { setFilterQuery } from '../redux/actions/filterQuery';
import { RootState } from '../redux/store';
import ChangeViewButton from './ChangeViewButton';

const useStyle = makeStyles({
  appBar: {
    backgroundColor: '#e46d07',
    marginBottom: 30,
    top: 100
  },
  errorAlert: {
    backgroundColor: '#ffc0cb',
    borderRadius: 0
  },
  navigationBtn: {
    border: '2px solid #ffffff',
    color: '#ffffff',
    fontWeight: 'bold',
    ['@media only screen and (max-width: 600px)']: {
      fontSize: 12
    },
  },
  favoritesBtn: {
    marginLeft: 8
  },
  toolbar: {
    justifyContent: 'space-between',
    ['@media only screen and (max-width: 600px)']: {
      minHeight: 80
    }
  },
  searchBar: {
    display: 'inline-flex',
    backgroundColor: '#fff',
    borderRadius: 4,
    padding: '0 8px',
    marginLeft: 8,
    '& > svg': {
      color: 'gray',
      marginTop: 4
    },
    ['@media only screen and (max-width: 600px)']: {
      marginLeft: 0,
      marginTop: 4
    }
  },
  searchField: {
    '& .MuiOutlinedInput-notchedOutline': {
      border: 'none'
    },
    '& .MuiOutlinedInput-input': {
      padding: 8
    }
  },
  clearIcon: {
    cursor: 'pointer'
  }
});

const selectFilterQuery = (state: RootState) => state.filterQuery;
const selectError = (state: RootState) => state.errors;

const getButtonData = (pathname?: string) => {
  if (!pathname) return null;
  
  switch (pathname) {
    case '/products':
      return { name: 'Categories', to: '/categories' };
    default:
      return { name: 'Products', to: '/products' };
  }
}

export default function NavBar() {
  const classes = useStyle();
  const dispatch = useDispatch();
  const filterQuery = useSelector(selectFilterQuery)
  const error = useSelector(selectError);
  
  const { pathname } = useLocation();
  const navButtonData = getButtonData(pathname);

  const closeAlert = () => dispatch(setError(''));

  return (
      <AppBar className={classes.appBar} position="sticky">
        {navButtonData && (
          <Toolbar className={classes.toolbar}>
            <Box>
              <Button
                className={classes.navigationBtn}
                component={NavLink}
                disabled={Boolean(error)}
                endIcon={<ForwardIcon />}
                size="small"
                to={navButtonData.to}
                variant="outlined"
              >
                {navButtonData.name}
              </Button>

              <Button
                className={`${classes.navigationBtn} ${classes.favoritesBtn}`}
                component={NavLink}
                disabled={Boolean(error)}
                endIcon={<ForwardIcon />}
                size="small"
                to='/favorites'
                variant="outlined"
              >
                FAVORITES
              </Button>

              <Box className={classes.searchBar}>
                <TextField
                  className={classes.searchField}
                  variant='outlined'
                  value={filterQuery ?? ''}
                  onChange={(e) => dispatch(setFilterQuery(e.target.value))}
                />
                {!filterQuery.length ? <Search /> : <Clear className={classes.clearIcon} onClick={() => dispatch(setFilterQuery(''))} />}
              </Box>
            </Box>
            {(pathname === '/products' || pathname === '/favorites') && !Boolean(error) && <ChangeViewButton />}
          </Toolbar>
        )}
        {error && (
          <Alert
            className={classes.errorAlert}
            severity="error"
            onClose={closeAlert}
            variant="outlined"
          >
            {error}
          </Alert>
        )}
      </AppBar>
  );
}
