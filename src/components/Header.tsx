import { fetchShopInfo } from '../redux/actions/thunks';
import { RootState } from '../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';

const selectShopInfo = (state: RootState) => state.shopInfo;

export default function Header() {
  const dispatch = useDispatch();
  const shopInfo = useSelector(selectShopInfo);

  useEffect(() =>{
    if (!shopInfo?.name) {
      dispatch(fetchShopInfo())
    }
  }, [shopInfo])

  return (
    <div className="header-container">
      {shopInfo?.name && (
        <header className="header">
          <h1 className="header__name">{shopInfo.name}</h1>
          <p className="header__description">
            {shopInfo.category}
            {' '}
            {<span>(back office)</span>}
          </p>
        </header>
      )}
    </div>
  );
}
