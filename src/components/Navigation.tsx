import { Redirect, Route, Switch } from 'react-router-dom';
import { lazy } from 'react';

const CategoriesPage = lazy(() => import('../pages/CategoriesPage'));
const NoPageFound = lazy(() => import('../pages/NoPageFound'));
const ProductPage = lazy(() => import('../pages/ProductPage'));
import ProductsContainer from '../containers/ProductsContainer';

export default function Navigation() {
  return (
    <Switch>
      <Route exact path="/">
        <Redirect to="products" />
      </Route>
      <Route exact path="/products" component={ProductsContainer} />
      <Route exact path="/favorites" component={ProductsContainer} />
      <Route exact path="/products/:id" component={ProductPage} />
      <Route exact path="/categories" component={CategoriesPage} />
      <Route exact path="/404" component={NoPageFound} />
      <Redirect to="/404" />
    </Switch>
  )
}
