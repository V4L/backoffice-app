import {
  Avatar,
  Box,
  Button,
  ButtonBase,
  Card,
  CardContent,
  CardHeader,
  Chip,
  makeStyles,
  Typography
} from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import Favorite from '@material-ui/icons/Favorite';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import { RootState } from '../redux/store';
import { Product } from '../redux/types';
import { addFavoritesToStorage, getFavoritesFromStorage, removeFavoritesFromStorage } from '../utils';
import ReviewStars from './Stars';

const useStyle = makeStyles({
  card: {
    borderColor: grey[300],
    borderStyle: 'solid',
    borderWidth: 1
  },
  cardAction: {
    alignItems: ({ view }: StyleProps) => view === 'grid' ? 'flex-start' : 'center',
    display: 'flex',
    flexDirection: ({ view }: StyleProps) => view === 'grid' ? 'column' : 'row',
    justifyContent: 'flex-start'
  },
  cardContent: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingBlockStart: 10,
    width: ({ view }: StyleProps) => view === 'grid' ? '100%' : '55%',
    ['@media only screen and (min-width: 768px)']: {
      flexDirection: ({ view }: StyleProps) => view === 'grid' ? 'column' : 'row',
      width: ({ view }: StyleProps) => view === 'grid' ? '100%' : '70%',
    }
  },
  cardContentGroup: {
    width: '100%',
    ['@media only screen and (min-width: 768px)']: {
      width: ({ view }: StyleProps) => view === 'grid' ? '100%' : '40%'
    }
  },
  cardHeader: {
    flexDirection: 'column',
    width: ({ view }: StyleProps) => view === 'grid' ? '100%' : '40%',
    ['@media only screen and (min-width: 375px)']: {
      alignSelf: 'flex-start',
      flexDirection: 'row'
    },
    ['@media only screen and (min-width: 768px)']: {
      width: ({ view }: StyleProps) => view === 'grid' ? '100%' : '30%'
    }
  },
  category: {
    backgroundColor: '#efa345',
    color: '#ffffff',
    marginTop: 8,
    marginBottom: 8
  },
  headerTitle: {
    fontSize: 12,
    fontStyle: 'italic',
    opacity: .5
  },
  title: {
    fontWeight: 'bold'
  },
  description: {
    fontSize: 12
  },
  subHeaderWrapper: {
    position: 'relative'
  },
  subHeader: {
    color: '#e46d07'
  },
  favoriteBtn: {
    color: ({ isFavorite }) => isFavorite ? 'red' : 'gray',
    cursor: 'pointer',
    position: 'absolute',
    top: -25,
    right: -20,
    ['@media only screen and (max-width: 600px)']: {
      top: ({ productsView }: StyleProps) => productsView === 'grid' ? -30 : 'unset',
      right: ({ productsView }: StyleProps) => productsView === 'grid' ? -30: 'unset',
      left: ({ productsView }: StyleProps) => productsView === 'panel' ? -68: 'unset',
    }
  }
});

const selectProductsView = (state: RootState) => state.productsView;

interface ProductProps {
  product: Product;
  view: string;
}

interface StyleProps {
  isFavorite: boolean;
  view: string;
  productsView: string;
}

export default function ProductComponent({ product, view }: ProductProps) {
  const productsView = useSelector(selectProductsView);
  const [isFavorite, setIsFavorite] = useState(getFavoritesFromStorage()?.some((f: Product) => f.id === product.id))
  const classes = useStyle({ view, isFavorite, productsView });
  const { pathname } = useLocation()
  
  const isFavoritesPage = pathname === '/favorites';

  return (
    <Card className={classes.card}>
      <ButtonBase
        className={classes.cardAction}
        component={Link}
        to={`/products/${product.id}`}
        onClick={(e: any) => e.stopPropagation()}
      >
        <CardHeader
          avatar={<Avatar>{product.employee[0].toLowerCase()}</Avatar>}
          className={classes.cardHeader}
          title={(
            <Typography
              component="p"
              className={classes.headerTitle}
              noWrap
            >
              created by
            </Typography>
          )}
          subheader={(
            <Box className={classes.subHeaderWrapper}>
              <Typography
                component="p"
                className={classes.subHeader}
                noWrap
                >
                {product.employee}
              </Typography>

              {!isFavoritesPage && (
                <Button
                  className={classes.favoriteBtn}
                  onClick={(e) => {
                    e.preventDefault();
                    if (isFavorite) {
                      setIsFavorite(false);
                      removeFavoritesFromStorage(product.id);
                    } else {
                      setIsFavorite(true);
                      addFavoritesToStorage(product);
                    }
                  }}
                >
                  <Favorite />
                </Button>
              )}
            </Box>
          )}
        />

        <CardContent className={classes.cardContent}>
          <Box className={classes.cardContentGroup}>
            <Typography className={classes.title} component="p" noWrap>
              {product.title.toUpperCase()}
            </Typography>
            <Typography className={classes.description} component="p" noWrap paragraph>
              {product.description}
            </Typography>
          </Box>
          <Box className={classes.cardContentGroup}>
            <Typography color="textPrimary" component="p">
              {`€/kg ${product.price}`}
            </Typography>
            <Chip label={product.category.toUpperCase()} size="small" className={classes.category} />
            <ReviewStars count={product.rating} />
          </Box>
        </CardContent>
      </ButtonBase>
    </Card>
  );
}
