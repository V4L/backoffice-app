import ClipLoader from 'react-spinners/ClipLoader';

interface ScreenLoaderProps {
  modifier?: string;
}

export default function ScreenLoader({ modifier }: ScreenLoaderProps) {
  return (
    <div className={`loader ${modifier && `loader--${modifier}`}`}>
      <ClipLoader color="#e46d07" />
    </div>
  );
} 