import AppsIcon from '@material-ui/icons/Apps';
import MenuIcon from '@material-ui/icons/Menu';
import { Button, makeStyles, Tooltip } from '@material-ui/core';
import { changeProductsView } from '../redux/actions/productsView';
import { RootState } from '../redux/store';
import { useDispatch, useSelector } from 'react-redux';

const useStyle = makeStyles({
  button: {
    border: '2px solid #ffffff',
    minWidth: 0,
    padding: 0,
    flex: 'none'
  },
  icons: {
    color: '#ffffff'
  }
})

const selectProductsView = (state: RootState) => state.productsView;

export default function ChangeViewButton() {
  const classes = useStyle();
  const dispatch = useDispatch();
  const currentView = useSelector(selectProductsView);

  const Icon = currentView === 'grid' ? MenuIcon : AppsIcon;

  const handleClick = () => {
    const newView = currentView === 'grid' ? 'panel' : 'grid';
    dispatch(changeProductsView(newView));
  }

  return (
    <Tooltip title="Change View" placement="top">
      <Button
        className={classes.button}
        onClick={handleClick}
        size="small"
      >
        <Icon fontSize="small" className={classes.icons} />
      </Button>
    </Tooltip>
  );
}
