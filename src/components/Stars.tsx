import range from 'lodash/range';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';
import { makeStyles } from '@material-ui/core';

const useStyle = makeStyles({
  icons: {
    color: "#efa345"
  }
});

interface StarsProps {
  count: number;
}

export default function Stars({ count }: StarsProps) {
  const classes = useStyle();
  const stars = range(0, 5).map((_item, index) => (
    index > count - 1
      ? <StarBorderIcon className={classes.icons} key={index} />
      : <StarIcon className={classes.icons} key={index} />
  ));

  return (
    <div className="stars">
      {stars}
    </div>
  )
}
