import { FC } from 'react';

export const MainContainer: FC = ({ children }) => {
  return (
    <div className="main-container">{children}</div>
  );
}

export default MainContainer;
