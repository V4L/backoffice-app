import { Box, Fab, makeStyles, Typography } from '@material-ui/core';
import Add from '@material-ui/icons/Add';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import Pagination from '../components/Pagination';
import ProductComponent from '../components/Product';
import withAddProduct from '../hocs/withAddProduct';
import { fetchProducts } from '../redux/actions/thunks';
import { RootState } from '../redux/store';
import { Product } from '../redux/types';
import { getFavoritesFromStorage } from '../utils';

const useStyle = makeStyles({
  fab: {
    backgroundColor: '#e46d07',
    '&:hover': {
      backgroundColor: '#e46d07'
    },
    bottom: '10vh',
    color: '#ffffff',
    left: '75vw',
    position: 'sticky',
    width: 60,
    ['@media only screen and (min-width: 768px)']: {
      borderRadius: 90,
      bottom: 50,
      left: '70vw',
      width: 180
    },
    ['@media only screen and (min-width: 900px)']: {
      left: '65vw'
    }
  },
  fabText: {
    display: 'none',
    ['@media only screen and (min-width: 768px)']: {
      display: 'block',
      marginLeft: 2
    }
  }
});

const selectProducts = (state: RootState) => state.products;
const selectProductsView = (state: RootState) => state.productsView;
const selectFilterQuery = (state: RootState) => state.filterQuery;

interface ProductsContainerProps {
  addProduct: () => void;
}

const paginatedProducts = (products:  Array<Product>, productsPerPage: number) => products?.reduce((newArray: Product[][], product, index) => {
  const newIndex = Math.floor(index / productsPerPage);
  newArray[newIndex] = [...(newArray[newIndex] ?? []), product];
  return newArray;
}, [])

const PRODUCTS_PER_PAGE = 6

function ProductsContainer({ addProduct }: ProductsContainerProps) {
  const classes = useStyle();
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const products = useSelector(selectProducts);
  const productsView = useSelector(selectProductsView);
  const filterQuery = useSelector(selectFilterQuery).toLowerCase();
  const [page, setPage] = useState(1);

  const isFavoritesPage = pathname === '/favorites';

  const currentLength = products?.list?.length;

  const items = !isFavoritesPage
    ? paginatedProducts(products.list, PRODUCTS_PER_PAGE)
    : paginatedProducts(getFavoritesFromStorage(), PRODUCTS_PER_PAGE)

  const filteredProducts = filterQuery
    ? (isFavoritesPage ? getFavoritesFromStorage() : products.list)
      ?.filter((p: Product) => p.title.toLowerCase().includes(filterQuery) || p.description.toLowerCase().includes(filterQuery))
    : items?.[page - 1]

  useEffect(() => {
    dispatch(fetchProducts())
  }, [])

  useEffect(() => {
    setPage(1)
  }, [isFavoritesPage])

  return (
    <Box className="products-container">
      <Box className={`products-container__items products-container__items--${productsView}`}>
        {filteredProducts?.map((p: Product) => (
          <ProductComponent
            key={p.id}
            product={p}
            view={productsView}
          />
        ))}
      </Box>
      
      {(!items || items.length === 0) && <Typography component='p' className='no-products-placeholder'>No Products</Typography>}

      {items?.length > 1 && !filterQuery && <Pagination currentPage={page} setPage={setPage} totalPages={items?.length} itemsPerPage={PRODUCTS_PER_PAGE} />}

      {currentLength > 0 && !isFavoritesPage && (
        <Fab
          className={classes.fab}
          onClick={addProduct}
          aria-label="add"
        >
          <Add />
          <Typography className={classes.fabText}>ADD PRODUCT</Typography>
        </Fab>
      )}
    </Box>
  );
}

export default withAddProduct(ProductsContainer);
